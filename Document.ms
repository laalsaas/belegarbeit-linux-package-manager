.nr PO 2.5c
.nr LL 16.5c
.nr HM 2.5c
.nr FM 2c
.nr PS 12p
.nr PD 1.5v
.ds FAM H
.nr no-head-numbering 1
.ds CH
.de ES \" end section
.  sp 2v \" adjust this as needed
..
.de CB \"begin code block
.LP
.DS L
.ft C
..
.de CE \"end code block
.ft
.DE
..
.Ff
\$3\**\$2
.FS
%a: %t. %w, %d
.FE
..
.Fr
..
.Fx
.FS
vgl. %a: %t. %w, %d
.FE
..
.ds CF %
.NH
.XN "Einleitung"
.NH 2
.XN "Wie installiert man Software?"
.LP
Unter Windows ist es üblich,
Software über sogenannte Installer zu installieren.
Installer sind ausführbare Dateien,
die die benötigten Dateien für das eigentliche Programm
mit Administratorrechten
in die entsprechenden Systemverzeichnisse kopieren.
Die Nutzer akquirieren diese Installer meist,
indem die Nutzer den Namen der Software,
die installiert werden soll,
in die Suchmaschine ihrer Wahl eingeben.
Dieser wird eventuell noch mit Zusätzen wie
\[Bq]installieren\[lq] oder \[Bq]herunterladen\[lq]
versehen, bevor einer der ersten Links ausgewählt wird.
Im Idealfall ist das entweder
die offizielle Seite des Softwareentwicklers oder
die eines seriösen Drittanbieters.
Von dieser Seite wird dann vom Nutzer
der Installer heruntergeladen.
In einem ungünstigerem Szenario wird man
auf eine bösartige Seite geleitetet,
die dem Nutzer vorgaukelt,
man könne hier den Installer für
die gewünschte Software herunterladen.
Man braucht kein abgeschlossenes Informatikstudium,
um zu erkennen,
dass es bösartigen Akteuren als
Einfallstor dienen könnte,
wenn Nutzer ein nach oberflächlichen Kriterien gewähltes
Programm herunterladen und mit Administratorrechten ausführen.
Dieses Gefahrenpotential kann durch Virenscanner
zwar eingeschränkt,
jedoch nicht eliminiert werden.
.LP
Ein weiteres Problem, welches dieses Methode eröffnet,
sind Updates.
Software ist nie fertig.
Es werden immer Sicherheitslücken und Bugs gefunden,
vielleicht werden sogar neue Funktionen umgesetzt.
Um sich über Updates bei Software,
welche mit der oben beschriebenen Methode installiert wurde
zu informieren,
müsste der Benutzer regelmäßig
auf der Webseite des Softwareentwicklers
nach einer neuen Version schauen und diese ggf. installieren.
Dies ist allerdings
in Anbetracht der Menge an Software,
die die meisten Nutzer installiert haben,
kaum umsetzbar.
Um dieses Problem zu adressieren gibt es auch wiederum Lösungsansätze.
So enthalten heute viele Programme die Funktion,
beim Programmstart die Website des Entwicklers zu kontaktieren
und auf neue Versionen zu überprüfen.
Wenn eine neuere Version gefunden wurde wird
im einfacheren Fall der Nutzer benachrichtigt.
In der vollständigeren Variante
wird die neue Version
automatisiert heruntergeladen und installiert.
Dies ist allerdings keinesfalls die Lösung aller Probleme,
da auch dies wieder neue Herausforderungen mit sich bringt.
So muss beispielsweise ein Programm,
dessen Fokus in der Entwicklung
auf der Konvertierung von Videoformaten liegt,
extra für solch eine \[Bq]Nebensächlichkeit\[lq]
Netzwerkkommunikation implementieren.
Daraus resultiert eine Feature-Überladung der Ursprungssoftware.
Gehen wir für unser Beispiel davon aus,
dass die Implementierung eines Autoupdaters 200 Codezeilen brauchen würde.
In unserem Beispiel wären dies 200 Codezeilen
für Netzwerkkommunikation,
die von Entwicklern geschrieben wurden,
deren Stärke/Fokus
auf dem Programmieren
von Videokonvertierungssoftware liegt.
Zudem wird bei der Instandhaltung der Codebasis
der Fokus mehr auf dem Code für die Videokonvertierung liegen
als auf dem Updater.
Deshalb sind solche Auto-Updater wiederum
eine Stelle,
an der Sicherheitslücken und Bugs
mit erhöhter Wahrscheinlichkeit auftreten können.
Nach dem Lesen dieses Abschnittes sollte klar sein,
dass die oben beschriebene Methode zur Softwareinstallation
keinesfalls optimal ist.
.ES
.NH 2
.XN "Eine Lösung: Paketmanager"
.LP
Ein Gutes ist,
dass diese Arbeit keineswegs die Erste ist,
die diese Probleme anerkennt
und Lösungen dafür sucht.
Insbesondere auf Linux- und UNIX-Systemen
gilt dieses Problem als gelöst.
Die Lösung heißt \[Bq]Paketmanager\[lq]
und existiert in ihren frühen Formen schon seit ca. 1993\**.
.Rx Katz 2017
.LP
Auch wenn der Begriff \[Bq]Paketmanager\[lq]
in der breiten Nutzerbasis kaum verbreitetet ist,
so benutzen doch viele Menschen
einen Paketmanager ohne es zu merken:
Auf ihren Smartphones.
Denn die sogenannten App-Stores
wie der Play Store auf Android
und der App Store auf iOS
sind nichts anderes als Paketmanager
mit einer graphischen Benutzeroberfläche.
Ein Paketmanager ist
.I ein
Programm, welches sich darum kümmert,
auf dem Betriebssystem
Pakete zu installieren,
zu deinstallieren,
zu aktualisieren
und ihre Metadaten wie Versionen,
Changelogs und Hilfedateien zu verwalten.
Ein Paket ist ein Programm
einschließlich aller seiner Metadaten und zugehörigen Dateien
in einem Dateiformat zusammengefasst,
welches ein Paketmanager lesen kann.
Paketmanager kamen in den 1990-er Jahren auf,
und waren teilweise ursprünglich simple Shellscripts,
um Dateien aus Archiven zu extrahieren
und hin- und herzukopieren\**.
.Rx Murdoc 1994
Heute sind es meist komplexe Programme,
die man sowohl über eine graphische Oberfläche,
als auch über ein Command-Line-Interface benutzen kann.
.ES
.NH 2
.XN "Warum gibt es mehrere Paketmanager?"
.LP
Wie oben bereits erwähnt,
gibt es nicht den
.I einen
Paketmanager.
Für Linux gibt es dpkg, pacman, nix, rpm, portage und viele mehr.
Allerdings hat eine Linux-Installation
normalerweise nur einen Paketmanager,
da die meisten Paketmanager
unter der Prämisse konzipiert wurden,
jeweils der exklusive Paketmanager
auf einem System zu sein.
Mehrere Paketmanager gleichzeitig zu verwenden,
bringt kaum einen Vorteil.
Es könnte aber zu ständigen Konflikten
zwischen den beiden Paketmanagern
auf einem System führen.
Wenn z.B. ein Paketmanager Version A eines Pakets installiert,
der andere Version b,
so könnte das entsprechende Paket
immer abwechselnd von den Paketmanagern
überschrieben werden.
.LP
Um den Sachverhalt erneut klarzustellen:
Es gibt n Paketmanager
und auf fast jedem Linux-System ist
.I Ein
Paketmanager installiert.
Aber warum gibt es überhaupt n Paketmanager,
wenn sie alle dieselbe Aufgabe erfüllen?
Es lässt sich eine gewisse Parallele ziehen
zwischen dieser Frage
und der Frage,
warum es mehrere Programmiersprachen gibt.
Es gibt nicht
.I "die eine"
Antwort auf diese Frage.
Es gibt aber viele Aspekte,
die eine Rolle spielen.
Zunächst: Technologie entwickelt sich konstant weiter.
Dies gilt natürlich auch für Paketmanager.
Heute ist es beispielsweise eine Selbstverständlichkeit,
dass ein Paketmanager die Abhängigkeiten
eines Programms automatisch auflöst und mitinstalliert.
Viele Paketmanager führen sogar Aufzeichnungen darüber,
warum ein Paket installiert wurde (Nutzerwillen
oder als Abhängigkeit eines anderen Pakets).
Sollte das Paket
nur als Abhängigkeit eines anderen Pakets
installiert worden sein,
so wird es automatisch deinstalliert,
sobald es keine Pakete mehr gibt,
die von diesem Paket abhängen.
An solch komplexe Funktionen
war in der Anfangszeit der Paketmanager,
in der die Paketmanager teilweise nur 900-zeilige Shellscripts waren\**,
.Rx Murdoc 1994
kaum zu denken.
.LP
Es gibt auch einige Paketmanager,
die auf speziellen Konzepten basieren,
die nicht für jeden Anwendungsfall geeignet sind.
So gibt es eine Vielzahl an Paketmanagern \[en]
und alle haben unterschiedliche Eigenschaften.
Die Wahl des Paketmanagers erfolgt selten vom Nutzer selbst,
sondern meist von den Entwicklern einer Linux-Distribution.
Die meisten Benutzer
merken wenig vom Funktionsumfang ihres Paketmanagers,
da doch ein Paketmanager meist nur für die drei Hauptfeatures genutzt wird:
Software installieren, aktualisieren und deinstallieren.
.LP
Ziel dieser Arbeit ist es,
einen etwas detaillierteren Einblick
in die allgemeine Funktionsweise
von zwei Paketmanagern zu geben:
pacman und apt.
Dabei soll auch eine Vorstellung von Paketierung,
also von dem Prozess,
der aus dem Quellcode einer Software
ein installierbares Paket erstellt, gewonnen werden.
Die Auswahl der Paketmanager
wurde unter folgenden Gesichtspunkten getroffen:
.IP \(bu
Pacman ist der Standardpaketmanager von Arch Linux,
einer Linux-Distribution, die sich an
technisch fortgeschrittene Nutzer richtet
und die sich primär dadurch auszeichnet,
dass es für nahezu jede erdenkliche Software ein
Paket im sogenannten Arch User Repository gibt
(detailliertere Erklärungen im entsprechenden Kapitel).
Daraus ergibt sich, dass der Prozess der Paketierung
für Pacman überdurchschnittlich einfach ist.
.IP \(bu
apt ist der Standardpaketmanager von Debian und
Ubuntu (welches auf Debian basiert).
Diese beiden Distributionen
gehören zu den meistgenutzten Linux-Distributionen
im Server- und im Anwendungsbereich.
Zudem ist Debian eine
der ältesten Linux-Distributionen,
die heute noch existieren\**.
.Rx Lundqvist 2012
.LP
Diese zwei Paketmanager decken ein möglichst breites Spektrum ab,
um Unterschiede und Gemeinsamkeiten zu entdecken.
.ES
.NH
.XN "Paketmanager vom Standpunkt eines Paketerstellers/-Verwalters"
.NH 2
.XN "Pacman (Arch Linux)"
.NH 3
.XN "Theoretische Überlegungen"
.LP
Dieses Kapitel
beschäftigt sich
mit der Erstellung
von Paketen für den Paketmanager
Pacman.
Pacman ist ein Kofferwort
aus den englischen Wörtern
"package" und "manager"
und hat nichts mit dem gleichnamigen Spiel zu tun.
Der Paketmanager Pacman ist der Standardpaketmanager
der Linux-distribution Arch Linux
sowie deren Derivate.
Pacman hat
wie alle hier behandelten Paketmanager
die Fähigkeit,
sich mit einem oder mehreren Servern (sogenannte Repositories)
zu synchronisieren und von dort Pakete zu installieren.
Um eigene Pakete zu erstellen,
Software zu kompilieren etc.
braucht man das Paket
.CW base-devel .
Dieses lässt sich mit dem Befehl
.CW "pacman -S base-devel"
installieren.
Dieser Befehl sorgt dafür,
dass Pacman das Paket,
welches in einem der Repositories vorkompiliert bereitliegt,
herunterlädt und installiert.
Diese Repositories werden von den Entwicklern
der Distribution
betrieben und verwaltet.
Verbreitete Software
lässt sich oft in den Standardrepositories finden,
allerdings können diese unmöglich alle Pakete beinhalten.
Für diesen Fall gibt es das Arch User Repository (AUR)\**.
.FS
https://aur.archlinux.org
.FE
Das AUR ist eine Paketsammlung,
zu der jeder seine eigenen Pakete beitragen kann,
der sich ein Benutzerkonto erstellt.
Daraus folgt allerdings auch,
dass Pakete im AUR nicht so gut überprüft werden
wie die in den Standardrepositories.
Bei Paketen aus dem AUR
ist also
eine gewisse Grundvorsicht geboten.
Eine technische Besonderheit des AURs besteht darin,
dass es (hauptsächlich aus Ressourcengründen)
nicht aus vorkompilierten Paketen besteht
sondern lediglich aus den Buildinstruktionen für Pakete,
welche sich der Nutzer selbst kompilieren muss.
Pacman-Pakete bestehen hauptsächlich aus einer Textdatei,
in welcher der Build-Prozess beschrieben ist,
welcher vom Arch Build System mit dem
.CW makepkg
Tool ausgeführt wird.
Um ein Paket aus dem AUR zu installieren,
kann der Nutzer diese Buildinstruktionen
herunterladen
und das Paket bauen und installieren.
Alternativ kann er auf einen sogenannten AUR Helper zurückgreifen,
ein Tool welches diese Vorgänge automatisiert.
Es gibt sowohl graphische\**
.FS
z.B. pamac (https://gitlab.manjaro.org/applications/pamac)
.FE
als auch kommandozeilenbasierte AUR Helper\**.
.FS
z.B. paru (https://github.com/morganamilo/paru)
.FE
.ES
.NH 3
.XN "Praktische Paketierung"
.LP
Hauptbestandteil zur Erstellung eines pacman-Pakets ist der
.CW PKGBUILD ,
eine Textdatei,
in der sowohl die Metadaten des Pakets liegen
als auch das Verhalten beim Build-
und Installationsprozess festgelegt wird.
Der
.CW PKGBUILD
wird bei der Erstellung des Paketes vom Ersteller des Pakets
(dem Paketierer) angelegt
und beim Bauen des Paketes von der bash-shell
(ein populärer Linux-Kommandozeileninterpreter) interpretiert.
Um einen besseren Einblick zu gewähren,
folgt hier die schrittweise Analyse eines
.CW PKGBUILD s,
welcher das Paket für ein hello-world Programm spezifiziert.
.CB
pkgname="hello-world"
pkgver="1.0"
pkgrel=1
arch=('any')
pkgdesc='A small hello world program written in C'
source=('https://codeberg.org/laalsaas/\[CR]
	belegarbeit-linux-package-manager/archive/main.tar.gz')
sha512sums=('SKIP')
depends=('glibc')
.CE
.LP
Diese Zeilen sind der Kopfteil eines
.CW PKGBUILD s,
in dem die Metadaten eines Paketes in dafür vorgesehenen
Variablen abgelegt werden.
Hier werden die mindestens erforderlichen Variablen,
um ein Paket zu erstellen festgelegt.
Es gibt jedoch noch viele andere,
um zusätzliche Daten zum Paket hinzuzufügen.
Die ersten beiden Variablen
geben Name und Version des Paketes an.
.LP
.CW pkgrel
ist kurz für \[Bq]release number\[lq] und wird genutzt,
um zwischen unterschiedlichen Versionen eines
.CW PKGBUILD s
zu unterscheiden,
die aber dieselbe Version der eigentlichen Software paketieren.
.LP
.CW pkgver
und
.CW pkgdel
haben unterschiedliche Aufgaben.
.CW pkgver
gibt die Version
der paketierten Software \[Bq]Upstream\[lq] an.
Upstream ist der Begriff für die Stelle,
an der die Software ursprünglich entwickelt wird.
Das heißt wenn beispielsweise ein
.CW PKGBUILD
für eine Libreoffice 7.0.4 vorliegt,
dann hat zunächst
.CW pkgver
den Wert von \[Bq]7.0.4\[lq]
und
.CW pkgrel
den Wert 1.
Sollte dem Paketierer beim Erstellen des
.CW PKGBUILD
ein Fehler unterlaufen sein,
dann kann er seinen Fehler korrigieren.
Eine häufige Quelle solcher Fehler ist,
dass vergessen wurde,
eine Abhängigkeit der Software anzugeben.
Wenn allerdings solch ein Fehler behoben wird
und ein korrigierter
.CW PKGBUILD
veröffentlicht wird,
existieren zwei
.CW PKGBUILD s
für dieselbe Version einer Software.
Um auch bei solchen Korrekturen
eine eindeutige Unterscheidung zu gewährleisten,
wird in diesem Fall mit der Wert von
.CW pkgrel
um den Wert Eins inkrementiert.
Dies passiert mit jeder neuen Version des
.CW PKGBUILD s,
welcher Build-Instruktionen für dieselbe
Version der eigentlichen Software enthält.
Sobald in unserem Beispiel Libreoffice 7.0.5 veröffentlicht würde,
würde
.CW pkgver
den Wert \[Bq]7.0.5\[lq] erhalten
und
.CW pkgrel
würde auf Eins zurückgesetzt.
.LP
.CW arch
ist ein Array (hier mit nur einem Element),
in dem die Prozessorarchitekturen hinterlegt werden.
In diesem Fall hat
.CW arch
den speziellen Wert \[Bq]any\[lq] bekommen,
da das hier paketierte Programm
keinerlei architekturspezifischen Code enthält.
.LP
.CW pkgdesc,
kurz für \[Bq]package description\[lq] enthält eine
.I kurze
Beschreibung des Pakets.
.CW source
ist ein weiteres Array.
.LP
In
.CW source
werden URLs von komprimierten Archiven abgespeichert.
Diese werden zu Beginn des Build-Prozesses automatisch heruntergeladen
und extrahiert.
Dies ist meist der eigentliche Quellcode
der paketierten Software,
aus dem dann das Paket gebaut wird.
In diesem Fall ist es ein Link zur aktuellsten Version
des Quellcodes dieser Arbeit
inklusive eines kleinen hello-world Programmes.
Codeberg, der Online-service,
der für die Sicherung des
Quellcodes dieser Arbeit verwendet wird,
bietet die Funktion, automatisch
aus der neuesten Version ein .tar.gz-archiv zu generieren
und zum Download bereitzustellen.
In
.CW source
liegt ebendiese URL,
wodurch beim Bauen des Paketes jedes mal
die neueste Version des Quellcodes heruntergeladen wird.
Normalerweise bevorzugt man eine spezifische Version
einer Software anzugeben.
Für dieses Projekt,
welches eher zu Demonstrationszwecken besteht,
ist stets die neuste Version jedoch ideal.
.LP
Die vorletzte hier definierte Variable,
.CW sha512sums
ist ein Array, welches die Prüfsummen der Dateien in
.CW source
als sha512-hashes beinhaltet.
In diesem Fall wird auf eine Prüfung jedoch
durch den spezialwert \[Bq]SKIP\[lq] verzichtet,
da sich dieses Projekt in konstanter Entwicklung
befindet. Sonst würde eine Prüfung hier jedes mal fehlschlagen sobald
sich irgendetwas am Projekt (und sei es nur am Dokument)
geändert hat.
.LP
Zuletzt gibt
.CW depends
an,
von welchen Paketen dieses Paket abhängt.
Dieses Hello-world-Programm
hängt nur von der C-Standardbibliothek <stdio.h>
ab, die vom Paket glibc zur Verfügung gestellt wird.
Die C-Standardbibliothek ist zwar auf jedem Linux-System
bereits installiert,
dennoch sollten der Vollständigkeit wegen
alle Abhängigkeiten angegeben werden.
.LP
Auf diesen Kopfteil folgt der Teil,
in dem die Anweisungen stehen,
mithilfe derer aus dem Quellcode ein Paket erstellt wird:
.CB
build() {
	cd $srcdir/belegarbeit-linux-package-manager/praxis/src
	gcc hello.c -o hello-world
}
package() {
	mkdir -p $pkgdir/usr/bin/
	install $srcdir/belegarbeit-linux-package-manager/\[CR]
		praxis/src/hello-world $pkgdir/usr/bin/
}
.CE
.LP
Hier werden die Funktionen\**
.FS
nach Terminologie des Informatiksystemeunterrichts: Prozeduren
.FE
.CW build()
und
.CW package()
deklariert.
Diese Funktionsnamen sind vorgeschrieben.
Für einen gültigen
.CW PKGBUILD
ist lediglich die
.CW package() -Funktion
zwingend erforderlich.
Da hier allerdings ein Kompilieren der Software nötig ist,
wird hier zusätzlich die optionale
(aber sehr häufig verwendete) Funktion
.CW build()
deklariert.
Es gibt noch einige andere optionale
Funktionen, z.B.
.CW check() ,
die hier aber nicht nötig sind.
Die Funktion
.CW build()
ist verantwortlich dafür,
die Binärdateien zu erstellen.
Dies tut sie,
indem sie zunächst in den Ordner,
in dem der eigentliche Quellcode liegt wechselt
und danach mit dem Compiler
.CW gcc
die Datei hello.c kompiliert.
In der Funktion
.CW package()
passiert der eigentlich wichtige Part:
dabei spielt die Variable
.CW $pkgdir
eine zentrale Rolle.
Sie wird nirgendwo im
.CW PKGBUILD
zugewiesen, da sie vom Build-system vordefiniert ist.
In
.CW $pkgdir
steht der Pfad eines Ordners,
welcher vom Build-system erstellt wurde.
Die eigentliche Aufgabe eines
.CW PKGBUILD s
besteht darin,
alle benötigten Dateien
in der Hierarchie
analog zu den Systemverzeichnissen
in diesem Ordner abzulegen.
Installierte Binärdateien liegen meist in
.CW /usr/bin/ ,
also installiert die
.CW build() -Funktion
diese auch nach
.CW $pkgdir/usr/bin/ .
Wenn dieses Programm eine ausführlichere Anleitung hätte,
dann müsste diese von der
.CW build() -Funktion
nach
.CW $pkgdir/usr/share/man/
installiert werden,
damit sie auf dem Zielsystem in
.CW /usr/share/man
gefunden werden kann.
Das Build-System erledigt den Rest:
Das komprimieren dieses Ordners
und das Hinzufügen der Metadaten (Version etc)
in einem standardisiertem Format.
Auf dem Zielsystem wird beim Installieren die Hierarchie des
.CW $pkgdir -Ordners
mit der Hierarchie der Systemverzeichnisse zusammengeführt.
.LP
Um solch ein Paket zu bauen und installieren,
muss man lediglich den
.CW PKGBUILD
besitzen
und in dem Verzeichnis
in welchem dieser gespeichert ist den Befehl
.CW "makepkg -s"
ausführen.
Damit dieser Build-Befehl den
.CW PKGBUILD
findet ist es notwendig,
dass der Dateiname immer noch
.CW PKGBUILD
lautet.
Dieser Build-Befehl installiert die notwendigen Dependencies
und erstellt eine Datei
im aktuellem Arbeitsverzeichnis
mit dem Namen
.CW <Paketname>-<Version>-<Architektur>.pkg.tar.zstd .
Diese Datei ist ein Pacman-Paket
welches mit dem Befehl
.CW "pacman -U <dateiname>"
installiert werden kann.
Wenn diese Schritte erfolgreich durchgeführt wurden,
dann kann die das Programm,
welches mithilfe des Pakets installiert wurde,
von jedem Nutzer des Systems mit dem Befehl
.CW hello-world
ausgeführt werden.
.ES
.NH 2
.XN "apt (Debian)"
.NH 3
.XN "Theoretische Überlegungen"
.LP
Apt ist der Standardpaketmanager von Debian.
Debian ist eine der ältesten Linux-Distributionen,
welche heute noch existieren und weiterentwickelt werden.
Debian setzt auf eine sehr langsame Release-Strategie.
Das heißt in Debian
gibt es nicht immer die neuesten Versionen
einer Software,
dafür wird sehr auf Stabilität geachtet.
Deshalb ist Debian sehr gut für Server geeignet
und wird in diesem Bereich häufig eingesetzt.
Die Strukturen von Debians Paketverwaltungssystem
sind über Jahre hinweg gewachsen.
Ein System das sich langsam entwickelt hat,
weist Unterschiede auf zu einem System,
welches \[Bq]auf dem Reißbrett\[lq] entstanden ist.
Debian stammt aus einer Zeit,
in der es noch keine Selbstverständlichkeit war,
das jeder Computer eine ständige (oder zumindest häufige)
Internetverbindung hatte.
Deshalb wurde
.CW dpkg ,
der ursprüngliche
Paketmanager für Debian
ohne Netzwerkfähigkeit
und Abhängigkeitsauflösung entwickelt.
Als es später immer mehr Nachfrage nach solchen Funktionen gab,
wurde Apt entwickelt.
Apt steht für Advanced Package Tool.
Apt wurde nicht als Ersatz,
sondern als 
.I Ergänzung
zu dem bis dahin üblichen
.CW dpkg
entwickelt.
Apt implementiert Netzwerkfunktionalität,
automatisches Auflösen von Abhängigkeiten und vieles mehr\**
.Rx Gloor 2021
Um beispielsweise das Paket
.CW build-essentials
zu installieren,
kann ein Nutzer den Befehl
.CW "apt install build-essentials"
verwenden.
Dabei sucht 
.CW apt
online
in den für das jeweilige System konfigurierten Quellen
nach einem Paket namens
.CW "build-essentials"
und lädt es herunter.
Dann fordert 
.CW apt
.CW dpkg
dazu auf,
das heruntergeladene Paket zu installieren.
Normalerweise haben Nutzer hauptsächlich mit
.CW apt
(oder einer GUI für 
.CW apt )
zu tun.
Bei Arch Linux ist ziemlich klar geregelt wie man Pakete installiert:
Entweder es ist in den Offiziellen Repos,
dann installiert man es direkt von dort mit
.CW "pacman -S <paketname>" ,
oder es ist im AUR,
dann baut man das Paket selbst (siehe Kapitel 2.1.2).
Bei Debian ist die Situation etwas vielfältiger.
Folgende Methoden gibt es,
ein Paket zu installieren
(sortiert nach Vorzugswürdigkeit):
.IP \(bu
Wenn ein Paket für die gewünschte Software in den Standard-Paketquellen zur Verfügung steht,
dann kann das Paket mit
.CW "apt install <paketname>"
installiert werden.
.IP \(bu
Der Herausgeber der Software stellt eine Drittquelle zur Verfügung.
Wie bereits oben erwähnt,
sucht apt bei den konfigurierten \[Bq]Paketquellen\[lq]
nach dem angefragten Paket.
Eine Paketquelle ist ein Server im Internet,
der über http(s) erreichbar ist und
.CW apt
auf Anfrage einen Index
von allen Paketen,
die der Server bereitstellt,
liefert.
Jedes mal wenn der Befehl
.CW "apt update"
ausgeführt wird,
fragt Apt von allen konfigurierten 
Servern diesen Index an und speichert ihn in einer lokalen Datenbank.
Wenn dann
.CW "apt install <paketname>"
aufgerufen wird,
dann liest apt aus dieser lokalen Datenbank
die URL des entsprechenden Pakets
und lädt es von dieser URL herunter.
Auf einem neu installiertem Debian-System
sind die einzigen konfigurierten Paketquellen
die Debian-standardquellen für die jeweilige Version.
Ubuntu, das ebenfalls Apt benutzt,
hat eigene Paketquellen,
die auf einem Ubuntu-System vorkonfiguriert sind.
Allerdings kann jeder eine Paketquelle betreiben.
Viele Entwickler,
die ihre Software
gern auf Debian-basierte Systeme verteilen möchten,
betreiben eine eigene Paketquelle die nur ihre eigene Software beinhaltet.
Diese Paketquellen werden Fremd- oder Drittquellen genannt.
Paketquellen werden an zwei Orten definiert:
In der Datei
.CW /etc/apt/sources.list
und in allen Dateien im Ordner
.CW /etc/apt/sources.list.d/ .
Apt-intern wird
.CW /etc/apt/sources.list
mit allen Dateien in
.CW /etc/apt/sources.list.d/ 
gleich behandelt.
Um eine Drittquelle hinzuzufügen,
kann der Nutzer einfach ihre URL
in
.CW /etc/apt/sources.list
hinzufügen
und danach
.CW "apt update"
ausführen,
um den Index der Drittquelle zu erhalten.
Um dann ein Paket aus dieser Fremdquelle zu installieren,
kann der Benutzer den Befehl
.CW "apt install <paketname>"
verwenden, genauso wie bei einem normalem Paket aus den Systemquellen.
Allerdings muss man selbstverständlich vorsichtig sein,
welche Quellen man zu seinem System hinzufügt.
Sonst kann auch diese Methode ein Einfallstor für Malware sein.
.IP \(bu
Im dritten Fall stellen die Entwickler der Software
einfach das fertig gebaute Paket im .deb Format bereit.
Das .deb Format ist das Format,
das
.CW dpkg -Pakete
haben.
Der Nutzer lädt sich von der Webseite des Softwareentwicklers die Datei herunter und installiert diese mit
.CW "dpkg --install <dateiname.deb>" .
Diese Methode hat allerdings
die beiden Hauptnachteile der in Kapitel 1.1 geschilderten Methode.
Der Benutzer muss sich selbst um Updates kümmern
und ist dabei auf seinen gesunden Menschenverstand angewiesen,
damit er keine Malware installiert.
.ES
.NH 3
.XN Praktische Paketierung
.LP
Der praktische Paketierungsprozess gestaltet sich
im Vergleich zu Pacman etwas aufwändiger.
Zunächst muss man das .tar.gz-Archiv
mit dem Quellcode der zu pakettierenden Software 
herunterladen und im selben Verzeichnis extrahieren.
Dann muss man im extrahierten Verzeichnis
einen Unterordner namens 
.CW debian
erstellen.
Dieser Unterordner muss folgende Dateien beinhalten:
.CB
debian
|-- changelog
|-- compat
|-- control
|-- copyright
`-- rules
.CE
.LP
Die Datei 
.CW changelog
beinhaltet eine Versionshistorie
in einem festgesetzten Textformat.
Dieses Textformat beinhaltet
\[em] selbstverständlich \[em]
Informationen über die Änderungen jeder Version.
Es beinhaltet aber auch Informationen über die Priorität eines updates,
was hilfreich sein kann um Security-Updates hervorzuheben.
.LP
In
.CW control
steht ein Großteil der Metadaten des Pakets:
.CB
Source: helloworld
Maintainer: philipp <p.neu@posteo.net>
Priority: optional
Build-Depends: debhelper (>=9.0)

Package: helloworld
Architecture: any
Depends: ${shlibs:Depends} ${misc:Depends}
Description: A small Hello World program written in C
.CE
.LP
Die meisten Felder sollten selbsterklärend sein.
.CW Depends
benutzt eine spezielle Syntax,
welche dazu dient
dass das hier verwendete debhelper Tool
die Dependencies automatisch bestimmt.
.CW copyright
ist eine Datei in der die Lizenzinformationen für sowohl die gesamte Software,
als auch die einzelnen Dateien maschinenlesbar festgehalten werden.
Da es sich hier lediglich um ein Beispiel handelt,
und das paketierte Programm keine Uhrheberrechtlichen Ansprüche erhebt,
wird in diesem Fall die datei
.CW copyright
einfach leer gelassen.
.LP
Die Datei
.CW rules
nimmt eine zentrale Bedeutung ein.
Hier werden die Schritte beim Bauen des Pakets definiert:
.CB
#!/usr/bin/make -f

%:
	dh $@
override_dh_auto_build:
	gcc praxis/src/hello.c -o praxis/src/hello-world

override_dh_auto_install:
	mkdir -p debian/helloworld/usr/bin/
	install "praxis/src/hello-world" "debian/helloworld/usr/bin/"

clean:
	[ -e praxis/src/hello-world ] && rm praxis/src/hello-world || exit 0
.CE
Die erste Zeile spezifiziert den Interpreter:
Diese Datei enthält build-Anweisungen im Format des
.CW make -Buildsystems.
Make ist das Standardbuildsystem auf UNIX-basierten Systemen.
Sogenannte Makefiles benutzen eine recht einfache Syntax.
Der part for dem Doppelpunkt
ist der Name eines sogenannten Targets.
Die eingerückte(n) Zeile(n) danach sind die Anweisungen die dieses Target erzeugen.
Das
.CW % -Target
fungiert als Platzhalter,
das heißt alle nicht explizit definierten Targets werden von
.CW %
ausgeführt.
Der Befehl der hier damit assoziiert wird ist
.CW "dh $@" .
.CW "$@"
steht für den Namen des Targets.
.CW dh
ist das Debhelper-Tool.
Debhelper wurde entwickelt um das Debian-packaging zu vereinfachen,
insbesondere den Inhalt von
.CW rules .
Debhelper kümmert sich um einen Großteil der targets und bestimmt die Build-Anweisungen dafür.
Dafür geht Debhelper davon aus dass das ursprüngliche Projekt auch schon ein Makefile hat.
Da dies hier nicht der Fall ist,
müssen einige Ziele noch manuell definiert werden.
Damit
.CW dh
diese erkennt wird den Namen der Targets
.CW override_dh_auto_
vorangestellt.
Die Targets selbst sind relativ ähnlich zu den entsprechenden Funktionen im
.CW PKGBUILD ,
nur das Zielverzeichnis ist in diesem Fall
.CW debian/<paketname> .
Dann gibt es noch das zusätzliche Target
.CW clean ,
welches bei Bedarf die generierten Binaries löscht.

.LP
Die letzte hier benötigte Datei ist
.CW compat .
In
.CW compat
steht lediglich die Zahl \[Bq]10\[lq].
Dies dient als Information für Debhelper,
mit welcher Version das Paket rückwärtskompatibel sein muss.
.LP
Um das Paket zu bauen,
muss der Befehl
.CW "debmake -us -uc"
ausgeführt werden.
Dies generiert die Datei
.CW helloworld_1.0-1_amd64.deb .
Diese Datei kann mit dem Befehl
.CW "dpkg --install helloworld_1.0-1_amd64.deb"
installiert werden.
Als Resultat kann jeder Benutzer den Befehl
.CW hello-world
benutzen,
um das mithilfe des Paketes
.CW helloworld
installierte Programm aufzurufen.
.ES
.NH 1
.XN "Fazit"
.
.
.
.
.
.
.
.
.
.bp
.NH
.XN "Anlagen"
.NH 2
.XN "Selbstständigkeitserklärung"
.LP
Ich erkläre,
dass ich die vorliegende Arbeit
selbstständig und ohne fremde Hilfe
verfasst und keine anderen Hilfsmittel
als angegeben verwendet habe.
Insbesondere versichere ich, dass
ich alle wörtlichen
und sinngemäßen Übernahmen
aus anderen Werken
als solche kenntlich gemacht habe.

.PS
line from (-4,0) to (0,0) dotted
move to (-4,-0.15)
box width 2.75 "Philipp Neumann, Dresden, \n[dy].\n[mo].\n[year]" invisible
line from (0,0) to (3,0) invisible
.PE
.NH 2
.XN "Erklärung zur Schulinternen Nutzung"
.LP
Ich erkläre mich damit einverstanden,
dass die von mir verfasste Belegarbeit
der schulinternen Öffentlichkeit
im vollen Umfang zugänglich gemacht werden kann.

.PS
line from (-4,0) to (0,0) dotted
move to (-4,-0.15)
box width 2.75 "Philipp Neumann, Dresden, \n[dy].\n[mo].\n[year]" invisible
line from (0,0) to (3,0) invisible
.PE
.
.Ra Katz, Jeremy
.Rt A brief history of package management
.Rw https://blog.tidelift.com/a-brief-history-of-package-management
.Rd 8.1.2022
.Ry 2017
.
.Ra Murdoc, Ian Ashley
.Rt dpkg-0.93beta.sh (Früheste auffindbare Version von dpkg)
.Rw https://www.dpkg.org/history/ancient/dpkg-0.93beta.sh
.Rd 8.1.2022
.Ry 1994
.
.Ra Lundqvist, Anreas und Rodic, Donjan
.Rt GNU/Linux Distribution Timeline
.Rw https://futurist.se/gldt/wp-content/uploads/12.10/gldt1210.svg
.Rw 8.1.2022
.Ry 2012
.
.Ra Arch-Wiki Mitwirkende
.Rt PKGBUILD (Eintrag im Arch Linux Wiki)
.Rw https://wiki.archlinux.org/title/PKGBUILD
.Rw 9.1.2022
.Ry 2021
.
.Ra Gloor, Jordan
.Rt What’s the Difference Between APT and dpkg in Ubuntu?
.Rw https://www.makeuseof.com/apt-vs-dpkg/
.Rd 5.2.2021
.Ry 2021
.ds TOC Inhaltsverzeichnis
.ds CF
.bp
.OH ''''
.TC
\" vim: spell spelllang=de_de,en
