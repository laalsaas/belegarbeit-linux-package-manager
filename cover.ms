.nr PO 2.5c
.nr LL 16.5c
.nr HM 2.5c
.nr FM 2c
.nr PS 12p
.nr PD 1.5v
.ds FAM H
.nr no-head-numbering 1
.ds CH
.\" TitelSeite

.TS
center box expand;
L S S S
C S S S
C S S S
C S S S
C S S S
C S S S
C S S S
L L S S.
T{
.PSPIC -R img/logobsz.eps
T}
T{
.sp 2c
.LG
.LG
.LG
.LG
.B "Belegarbeit"
.sp 2c
T}
T{
.LG
.LG
.LG
.LG
.B "Paketmanager unter Linux"
.sp 2c
T}
T{
.LG
von
.sp
T}
T{
.LG
Philipp Neumann
.sp 2
T}
T{
.LG
Klasse
.sp
T}
T{
.LG
BGY20
.sp 5c
T}
T{
.LG
Fach:
T}	T{
.LG
Informatiksysteme
.sp
T}
T{
.LG
Betreuer:
T}	T{
.LG
Herr Morgenstern
.sp
T}
T{
.LG
Ort, Datum:
T}	T{
.LG
Dresden, \n[dy].\n[mo].\n[year]
.sp 3
T}
.TE
